import os
import socket
import subprocess

data_path = os.path.join(path, ".data")
cert_path = os.path.join(data_path, "certs")
bin_path = os.path.join(path, tags, "bin")
python_path = os.path.join(path, tags, "python")

env("PATH").insert(bin_path)
env("PYTHONPATH").insert(python_path)
env("MINIO_ROOT_USER").setenv("storage")
env("MINIO_ROOT_PASSWORD").setenv("storage!")

cmd_list = [
    "minio",
    "server",
    data_path,
    "--address",
    "localhost:9000",
    "--console-address",
    "localhost:9010",
    "--anonymous",
    "--quiet",
    #    "--certs-dir",
    #    cert_path,
]
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
if sock.connect_ex(("localhost", 9010)):
    print("Run CMD:", (" ").join(cmd_list), flush=True)
    subprocess.Popen(cmd_list)
