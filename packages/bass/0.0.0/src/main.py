import os
import sys

from env import Environ

config_name = "conf.py"
package_name = "package.py"

configure_env = "BASS_CONFIGURE_FILE"
thirdparty_env = "BASS_THIRDPARTY_DIRS"

message = ("Error: {}\nUndefined type\n")

message1 = (
    "Error: No parameter found\n"
    "You must specify package name\n"
    "Example format:\n"
    "bass name(must)@path(optional)=tags(optional) - cmd(optional)\n"
    "You can use + replace - to get verbose information\n"
)

message2 = (
    "Error: Thirdparty configure not exists\n"
    "Please set and check environment variables "
    "BASS_THIRDPARTY_DIRS or BASS_CONFIGURE_FILE\n"
)

message3 = ("Error: Path, Tags = {}\n"
            "Please check path or tags\n"
            "Tags or path cannot be an empty string\n")

message4 = (
    "Error: Can not found package\n"
    "Please check package configure\n"
    "Package: {}\nPackage name config "
    "not exists current thirdparty dirs\n"
)


class Resolve(object):
    package_list = list()

    def __new__(cls, *args):
        if not hasattr(cls, cls.__name__):
            setattr(cls, cls.__name__, object.__new__(cls))
        return getattr(cls, cls.__name__)

    def __init__(self, *args):
        if not args:
            self.exit_message(1)
        if not self.resolve_env():
            self.exit_message(2)

    def exit_message(self, exit_type, *args):
        if exit_type == 1:
            message = message1
        elif exit_type == 2:
            message = message2
        elif exit_type == 3:
            message = message3
        elif exit_type == 4:
            message = message4
        message = message.format(str(args))
        sys.stderr.write(message)
        os._exit(exit_type)

    def resolve_env(self):
        if os.environ.get(thirdparty_env):
            return True
        if not os.environ.get(configure_env):
            return False
        configure_file = os.environ.get(configure_env)
        modules = {"env": Environ, "__file__": configure_file}
        exec(open(configure_file, "rb").read(), modules)
        if os.environ.get(thirdparty_env):
            return True

    def resolve_argv(self, argv):
        path, tags = None, None
        p_count = argv.count("@")
        v_count = argv.count("=")
        if p_count == 0 and v_count == 0:
            name = argv
        elif p_count >= 1 and v_count == 0:
            name, path = argv.split("@", 1)
        elif p_count == 0 and v_count >= 1:
            name, tags = argv.split("=", 1)
        else:
            name_str, path_str = argv.split("@", 1)
            if "=" in name_str:
                name, tags = name_str.split("=", 1)
                path = path_str
            else:
                path, tags = path_str.split("=", 1)
                name = name_str
        return name, path, tags

    def resolve_path(self, name, path, tags):
        if path:
            if os.path.exists(path):
                path, tags, tags_list = self.resolve_pkgs(path, tags)
                if path:
                    return path, tags, tags_list
        else:
            third_partys = os.environ.get(thirdparty_env).split(os.pathsep)
            for thirdparty in third_partys:
                if not os.path.exists(thirdparty):
                    continue
                path = os.path.join(thirdparty, name)
                if not os.path.exists(path):
                    continue
                path, tags, tags_list = self.resolve_pkgs(path, tags)
                if path:
                    return path, tags, tags_list
        return None, None, None

    def resolve_pkgs(self, path, tags):
        tags_list = list()
        tags, flag = self.resolve_sign(tags)
        if tags is None or flag is not None:
            for i in os.listdir(path):
                if i.startswith('.'):
                    continue
                if os.path.isdir(os.path.join(path, i)):
                    tags_list.append(i)
            if tags_list:
                tags_list.sort(key=lambda x: [int(u) for u in x.split(".") if u.isdigit()])
                tags = self.resolve_tags(tags, flag, tags_list)
        if tags:
            package_file = os.path.join(path, tags, package_name)
            if os.path.exists(package_file):
                path = os.path.join(path, tags)
                return path, tags, tags_list
        package_file = os.path.join(path, package_name)
        if os.path.exists(package_file):
            return path, tags, tags_list
        return None, None, None

    def resolve_sign(self, tags):
        flag = None
        if tags:
            if tags.startswith("+"):
                tags = tags.strip("+")
                flag = "min+"
            if tags.startswith("-"):
                tags = tags.strip("-")
                flag = "max-"
            if tags.endswith("+"):
                tags = tags.strip("+")
                flag = "max+"
            if tags.endswith("-"):
                tags = tags.strip("-")
                flag = "min-"
        return tags, flag

    def resolve_tags(self, tags, flag, tags_list):
        tags_list = list(tags_list)
        if tags is None:
            return tags_list[-1]
        if tags not in tags_list:
            tags_list.append(tags)
        tags_list.sort(key=lambda x: [int(u) for u in x.split(".") if u.isdigit()])
        idx = tags_list.index(tags)
        if flag == "max+":
            if tags != tags_list[-1]:
                return tags_list[-1]
        if flag == "max-":
            if tags != tags_list[0]:
                return tags_list[0]
        if flag == "min+":
            if tags != tags_list[-1]:
                return tags_list[idx + 1]
        if flag == "min-":
            if tags != tags_list[0]:
                return tags_list[idx - 1]


class Request(Resolve):
    def __init__(self, *args):
        super(Request, self).__init__(*args)
        for argv in args:
            if argv in self.package_list:
                continue
            self.package_list.append(argv)
            self.exec_package(argv)

    def exec_package(self, argv):
        name, path, tags = self.resolve_argv(argv)
        if str() in (path, tags):
            self.exit_message(3, path, tags)
        path, tags, tags_list = self.resolve_path(name, path, tags)
        if None in (path, tags):
            self.exit_message(4, name, tags)
        file_path = os.path.join(path, package_name)
        if verbose:
            sys.stdout.write("\n")
            sys.stdout.write("Name:      {}\n".format(name))
            sys.stdout.write("Tags:      {}\n".format(tags))
            sys.stdout.write("TagsList:  {}\n".format(tags_list))
            sys.stdout.write("FilePath:  {}\n".format(file_path))
        modules = {
            "req": type(self),
            "env": Environ,
            "tags": tags,
            "path": path,
            "__file__": file_path,
        }
        exec(compile(open(file_path, "rb").read(), file_path, "exec"), modules)


def main():
    global verbose
    verbose = False
    mark_list = ("-", "+")
    argv_list = sys.argv[1:]
    command = os.environ.get("SHELL")
    mark_info = {argv_list.index(i): i for i in mark_list if i in argv_list}
    if not os.environ.get(configure_env):
        path, name = os.path.split(sys.argv[0])
        os.environ[configure_env] = os.path.join(path, config_name)
    if mark_info:
        i = mark_info.get(sorted(mark_info.keys())[0])
        command_list = argv_list[argv_list.index(i) + 1:]
        argv_list = argv_list[:argv_list.index(i)]
        command = (" ").join(command_list)
        if i == "+":
            verbose = True
    Request(*argv_list)
    os.system(command)


if __name__ == "__main__":
    main()
