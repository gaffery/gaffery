import os
from env import Environ as env

default_packages = os.path.realpath(__file__)
default_packages = os.path.dirname(os.path.dirname(default_packages))
default_packages = os.path.dirname(os.path.dirname(default_packages))

env('BASS_THIRDPARTY_DIRS').insert(default_packages)
