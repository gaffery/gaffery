import os
import sys


class Environ(object):
    def __new__(cls, *args):
        if not hasattr(cls, cls.__name__):
            setattr(cls, cls.__name__, object.__new__(cls))
        return getattr(cls, cls.__name__)

    def __init__(self, name):
        self.__ename = name
        self.__elist = list()
        if os.environ.get(name):
            self.__elist = os.environ.get(name).split(os.pathsep)

    def environ_set(func):
        def wrapper(self, *args):
            if not args:
                return False
            func(self, *args)
            if self.__elist:
                target_env = list(set(self.__elist))
                target_env.sort(key=self.__elist.index)
                target_env = list(map(os.path.expandvars, target_env))
                os.environ[self.__ename] = os.pathsep.join(target_env)
            else:
                self.unset()
            return True

        return wrapper

    def envlist(self):
        return self.__elist

    def getenv(self):
        if self.__elist:
            return os.pathsep.join(self.__elist)

    def unset(self):
        if os.environ.get(self.__ename):
            return os.environ.pop(self.__ename)

    @environ_set
    def setenv(self, *args):
        self.__elist = list(args)

    @environ_set
    def insert(self, *args):
        self.__elist[:0] = args

    @environ_set
    def append(self, *args):
        self.__elist.extend(args)

    @environ_set
    def remove(self, *args):
        self.__elist = [i for i in self.__elist if i not in args]

    @environ_set
    def unload(self, *args):
        self.__elist = [i for i in self.__elist if not i.startswith(args)]
