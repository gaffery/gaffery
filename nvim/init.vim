syntax on                             
filetype plugin indent on
highlight Pmenu ctermfg=0 ctermbg=7  
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1
set nowrap
set number
set viminfo=
set shell=bash
set foldmethod=indent
set foldlevel=99
set termguicolors
set guioptions-=r 
set guioptions-=L
set guioptions-=b
set showtabline=0                 

set background=dark               
set fileformat=unix               
set expandtab                     
set autoindent                    
set tabstop=4                     
set softtabstop=4                 
set shiftwidth=4                  
set showmatch                     
set scrolloff=5                   
set laststatus=2                  
set fenc=utf-8                    
set backspace=2
set mouse=a                       
set selection=exclusive
set selectmode=mouse,key
set matchtime=5
set ignorecase                    
set incsearch
set hlsearch                      
set noexpandtab                   
set whichwrap+=<,>,h,l
set backupdir=/tmp
set directory=/tmp
set completeopt=menu,menuone
if exists("g:neovide")
	set guifont=Hack:h18
	colorscheme peachpuff
	let g:neovide_transparency=1
	let g:neovide_cursor_animation_length=0
	let g:neovide_remember_window_size = v:true
endif

let $VIMCONFIG=expand('<sfile>:p:h')
source $VIMCONFIG/autoload/plug.vim

call plug#begin(expand('$VIMCONFIG/plugged'))
Plug 'Valloric/YouCompleteMe', {'do': './install.py --clang-completer'}
Plug 'scrooloose/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'Yggdroot/indentLine'
call plug#end()

let NERDTreeChDirMode = 1
let NERDTreeShowBookmarks = 0
let NERDTreeIgnore = ['\~$', '\.pyc$', '\.swp$']
let NERDTreeWinSize = 20
let g:NERDTreeWinPos = "right"
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDToggleCheckAllLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:ycm_server_python_interpreter = '/usr/bin/python'
let g:ycm_python_binary_path = '/usr/bin/python'
let g:ycm_cache_omnifunc = 0
let g:ycm_confirm_extra_conf = 0  
let g:ycm_show_diagnostics_ui = 0
let g:ycm_complete_in_comments = 1
let g:ycm_add_preview_to_completeopt = 0
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_key_invoke_completion = '<C-Space>'
let g:ycm_semantic_triggers = {'c,cpp,python,java,go,erlang,perl': ['re!\w{2}'],'cs,lua,javascript': ['re!\w{2}']}
let g:ycm_global_ycm_extra_conf = expand('$VIMCONFIG/plugged/YouCompleteMe/third_party/ycmd/examples/.ycm_extra_conf.py')
let g:lightline = {'active': {'right': [['lineinfo'],['percent'],['filetype']]}}
let maplocalleader = "\ "


func! CompileRun()
	exec "w"
	silent !clear
	if &filetype == 'c'
		exec '!cc -g -Wall % -o %< && ./%<'
	elseif &filetype == 'python'
		exec '!python %'
	elseif &filetype == 'sh'
		exec '!bash %'
	endif
endfunc

func! AutoFormat()
	exec "w"
	silent !clear
	if &filetype == 'c'
		exec "0,$!clang-format -style='{BasedOnStyle: WebKit, ColumnLimit: 120}'"
	elseif &filetype == 'python'
		exec "0,$!yapf --style='{based_on_style: facebook, column_limit: 120}'"
	endif
endfunc

noremap <F9> :NERDTreeToggle<CR>
vnoremap <F9> <ESC>:NERDTreeToggle<CR>
inoremap <F9> <ESC>:NERDTreeToggle<CR>
cnoremap <F9> <ESC>:NERDTreeToggle<CR>

noremap <F10> m0: call AutoFormat()<CR> `0
vnoremap <F10> m0: call AutoFormat()<CR> `0
inoremap <F10> <ESC> m0:call AutoFormat()<CR> `0
cnoremap <F10> <ESC> m0:call AutoFormat()<CR> `0

noremap <F12> :call CompileRun()<CR>
vnoremap <F12> :call CompileRun()<CR>
inoremap <F12> <ESC>:call CompileRun()<CR>
cnoremap <F12> <ESC>:call CompileRun()<CR>


noremap <c-c> "+y
noremap <c-v> "+p
vnoremap <c-c> "+y
inoremap <c-r> <c-v>
inoremap <c-v> <c-r>+
cnoremap <c-v> <c-r>+

noremap <leader>g mu:YcmCompleter GoToDefinitionElseDeclaration<CR>
