# Defined in - @ line 1
function tk --wraps=xclip --description 'tmux kill-server'
    tmux kill-server;
end
