function fish_prompt
  switch $fish_bind_mode
    case visual
      echo -n (set_color red)' →_→ '
    case insert
      echo -n (set_color green)' →_→ '
    case '*'
      echo -n (set_color blue)' →_→ '
  end
  set_color normal
end
