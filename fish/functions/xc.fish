# Defined in - @ line 1
function xc --wraps=xclip --description 'used xclip copy string to clipboard'
    tr -d "\n"|xclip -sele c $argv;
end
