# Defined via `source`
function gv --wraps='emacsclient -nc' --description 'alias gvim=emacsclient -nc'
  emacsclient -nc $argv; 
end
