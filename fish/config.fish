set fish_greeting
set -gx TERM xterm-256color
fish_add_path $HOME/.config/scripts/bin

if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startxfce4
    end
else
    if not set -q TMUX
        exec tmux
    end
end
